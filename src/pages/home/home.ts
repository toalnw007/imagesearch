import { Component } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Http, Headers, RequestOptions } from '@angular/http';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html',

})

export class HomePage {
  respon: any;
  currentImage: any;
  default_img: any;
  title:string;
  constructor(
    private http: Http,
    private camera: Camera,
    private imagePicker: ImagePicker
  ) {
    this.default_img = '../../assets/imgs/logo.jpg';
    this.currentImage = this.default_img;
    this.title = "true";
  }
  openCamera() {
    const options: CameraOptions = {
      targetWidth: 640,
      targetHeight: 640,
      quality: 80,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    }
    this.camera.getPicture(options).then((imageData) => {
      this.currentImage = 'data:image/jpeg;base64,' + imageData;
      this.respon = '';
      this.title = 'true';
    }, (err) => {
      console.log("Camera issue:" + err);
    });
  }
  // openGallery() {
  //   let options = {
  //     maximizeImageCount: 1,
  //     width: 640, 
  //     height: 640,
  //     quality: 80,
  //     outputType: 1
  //   }
  //   this.imagePicker.getPictures(options).then((results) => {
  //     for (var i = 0; i < results.length; i++) {
  //       console.log('Image URI: ' + results[i]);
  //       this.currentImage = 'data:image/jpeg;base64,' + results[i];
  //       this.respon = '';
  //       this.title = 'true';
  //     }
  //   }, (err) => { });
  // }
  async uploadImg() {
    if (this.currentImage != this.default_img) {
      let headers = new Headers({
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'Content-Type,Authorization',
        'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE'
      });
      let options = new RequestOptions({ headers: headers });
      let url = "https://polar-shore-46461.herokuapp.com/upload"
      // let url = "http://192.168.1.9:5000/upload"
      let postData = new FormData();
      postData.append("image", this.currentImage)
      this.http.post(url, postData, options).subscribe(data => {
        this.respon = data.json()
        console.log(data)
        this.title = '';
      }, (error) => {
        console.log(error)
      })
    }
  }
}